/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vista;

import Controlador.Vehiculos.VehiculoController;
import Vista.Tablas.TablaVehiculos;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class Vehiculos extends javax.swing.JDialog {
    private VehiculoController vehiculoController = new VehiculoController();
    private TablaVehiculos tv = new TablaVehiculos();
    DefaultTableModel model = new DefaultTableModel();
    private int pos = -1;
    /**
     * Creates new form Vehiculos
     */
    public Vehiculos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarTabla();
        btn_nuevos.setVisible(true);
        model.addColumn("Marca");
        model.addColumn("Color");
        model.addColumn("Precio");
        model.addColumn("Año");
        model.addColumn("Placa");
        this.tbl_tabla.setModel(model);
        
    }
    
    private void cargarTabla(){
        tv.setLista(vehiculoController.getListaVehiculos());
        tbl_tabla.setModel(tv);
        tbl_tabla.updateUI();
    }
    
    private void Limpiar() {
        txt_Marca.setText("");
        txt_Color.setText("");
        txt_Precio.setText("");
        txt_año.setText("");
        txt_Placa.setText("");
        vehiculoController.setVehiculo(null);
        pos = -1;
        cargarTabla();
        
        
    }
    //Guardar los datos
    public void guardar(){
        if (txt_Marca.getText().trim().isEmpty() || txt_Color.getText().trim().isEmpty() || txt_Precio.getText().trim().isEmpty()
                || txt_año.getText().trim().isEmpty() || txt_Placa.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "No hay componentes", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            vehiculoController.getVehiculo().setMarca(txt_Marca.getText().trim());
            vehiculoController.getVehiculo().setColor(txt_Color.getText().trim());
            vehiculoController.getVehiculo().setPrecio(txt_Precio.getText().trim());
            vehiculoController.getVehiculo().setAnio(txt_año.getText().trim());
            vehiculoController.getVehiculo().setPlaca(txt_Placa.getText().trim());
            //int pos=(vehiculocontroller.getListaVehiculos().getSize()==0) ?  0 : vehiculocontroller.getListaVehiculos().getSize()-1;
            if (pos == -1) {
                if (vehiculoController.getListaVehiculos().insertarAlFinal(vehiculoController.getVehiculo())) {
                    JOptionPane.showMessageDialog(null, "Se guardo", "OK", JOptionPane.INFORMATION_MESSAGE);
                    Limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se guardo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                if (vehiculoController.getListaVehiculos().modificarDatoPosicion(pos, vehiculoController.getVehiculo())) {
                    JOptionPane.showMessageDialog(null, "Se ha modificado", "OK", JOptionPane.INFORMATION_MESSAGE);
                    Limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo modificar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    //modificar datos
    private void modificar() {
        int fila = tbl_tabla.getSelectedRow();
        if (fila >= 0) {
            vehiculoController.setVehiculo(tv.getLista().obtenerDato(fila));
            txt_Marca.setText(vehiculoController.getVehiculo().getMarca());
            txt_Color.setText(vehiculoController.getVehiculo().getColor());
            txt_Precio.setText(vehiculoController.getVehiculo().getPrecio());
            txt_año.setText(vehiculoController.getVehiculo().getAnio());
            txt_Placa.setText(vehiculoController.getVehiculo().getPlaca());
            pos = fila;
        } else {
            JOptionPane.showMessageDialog(null, "Elija un comando de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
     private void eliminar() {
       int fila = tbl_tabla.getSelectedRow();
       if(fila>=0){
           vehiculoController.getVehiculo();
           txt_Marca.setText(vehiculoController.getVehiculo().getMarca());
            txt_Color.setText(vehiculoController.getVehiculo().getColor());
            txt_Precio.setText(vehiculoController.getVehiculo().getPrecio());
            txt_año.setText(vehiculoController.getVehiculo().getAnio());
            txt_Placa.setText(vehiculoController.getVehiculo().getPlaca());
            pos = fila;
       }else{
           JOptionPane.showMessageDialog(null, "Elija un comando de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
       }
    }
     
     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txt_Marca = new javax.swing.JTextField();
        txt_Color = new javax.swing.JTextField();
        txt_Precio = new javax.swing.JTextField();
        txt_año = new javax.swing.JTextField();
        txt_Placa = new javax.swing.JTextField();
        btn_limpiar = new javax.swing.JButton();
        btn_agregar = new javax.swing.JButton();
        btn_eliminar = new javax.swing.JButton();
        btn_nuevos = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("VEHICULOS");

        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("AdministracionVehiculos"));
        jPanel2.setLayout(null);

        jLabel1.setText("Marca :");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(20, 30, 70, 14);

        jLabel2.setText("Color:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(20, 70, 50, 14);

        jLabel3.setText("Precio: ");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(20, 110, 50, 14);

        jLabel4.setText("Año:");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(20, 150, 50, 14);

        jLabel5.setText("Placa:");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(20, 190, 50, 14);
        jPanel2.add(txt_Marca);
        txt_Marca.setBounds(100, 20, 210, 30);
        jPanel2.add(txt_Color);
        txt_Color.setBounds(100, 60, 210, 30);
        jPanel2.add(txt_Precio);
        txt_Precio.setBounds(100, 100, 210, 30);
        jPanel2.add(txt_año);
        txt_año.setBounds(100, 140, 210, 30);
        jPanel2.add(txt_Placa);
        txt_Placa.setBounds(100, 180, 210, 30);

        btn_limpiar.setText("LIMPIAR AUTOS");
        btn_limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limpiarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_limpiar);
        btn_limpiar.setBounds(340, 170, 170, 23);

        btn_agregar.setText("AGREGAR AUTOS");
        btn_agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_agregarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_agregar);
        btn_agregar.setBounds(340, 20, 170, 23);

        btn_eliminar.setText("ELIMINAR AUTOS");
        btn_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_eliminar);
        btn_eliminar.setBounds(340, 120, 170, 23);

        btn_nuevos.setText("NUEVOS AUTOS");
        btn_nuevos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevosActionPerformed(evt);
            }
        });
        jPanel2.add(btn_nuevos);
        btn_nuevos.setBounds(340, 70, 170, 23);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 550, 230);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("ListadoVehiculos"));
        jPanel3.setLayout(null);

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(20, 30, 510, 100);

        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);
        jButton1.setBounds(440, 130, 55, 23);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 250, 550, 170);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 570, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_agregarActionPerformed
        // TODO add your handling code here:
        String []agregar=new String[5];
        agregar[0]=txt_Marca.getText();
        agregar[1]=txt_Color.getText();
        agregar[2]=txt_Precio.getText();
        agregar[3]=txt_año.getText();
        agregar[4]=txt_Placa.getText();
        model.addRow(agregar);
    }//GEN-LAST:event_btn_agregarActionPerformed

    private void btn_limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limpiarActionPerformed
        // TODO add your handling code here:
        int elitotal=tbl_tabla.getRowCount();
        for(int i=elitotal-1; i>=0; i--){
            model.removeRow(i);
        }
    }//GEN-LAST:event_btn_limpiarActionPerformed

    private void btn_nuevosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevosActionPerformed
        // TODO add your handling code here:
        txt_Marca.setText("");
        txt_Color.setText("");
        txt_Precio.setText("");
        txt_año.setText("");
        txt_Placa.setText("");
    }//GEN-LAST:event_btn_nuevosActionPerformed

    private void btn_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarActionPerformed
        // TODO add your handling code here:
        int eli=tbl_tabla.getSelectedRowCount();
        if(eli>=0){
            model.removeRow(eli);
        }else{
            JOptionPane.showMessageDialog(null, "No hay datos que eliminar");
        }
    }//GEN-LAST:event_btn_eliminarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vehiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vehiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vehiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vehiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Vehiculos dialog = new Vehiculos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_agregar;
    private javax.swing.JButton btn_eliminar;
    private javax.swing.JButton btn_limpiar;
    private javax.swing.JButton btn_nuevos;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_tabla;
    private javax.swing.JTextField txt_Color;
    private javax.swing.JTextField txt_Marca;
    private javax.swing.JTextField txt_Placa;
    private javax.swing.JTextField txt_Precio;
    private javax.swing.JTextField txt_año;
    // End of variables declaration//GEN-END:variables
}
