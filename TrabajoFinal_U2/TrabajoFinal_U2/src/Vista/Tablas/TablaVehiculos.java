/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vista.Tablas;

import Modelo.Vehiculo;
import controlador.tda.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaVehiculos extends AbstractTableModel {
    private ListaEnlazadaServices<Vehiculo> lista;

    public ListaEnlazadaServices<Vehiculo> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Vehiculo> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Nro";
            case 1: return "Marca";
            case 2: return "Color";
            case 3: return "Precio";
            case 4: return "Año";
            case 5: return "Placa";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        Vehiculo vehiculo = lista.obtenerDato(arg0);
        switch(arg1) {
            case 0: return (arg0+1);
            case 1: return vehiculo.getMarca();
            case 2: return vehiculo.getColor();
            case 3: return vehiculo.getPrecio();
            case 4: return vehiculo.getAnio();
            case 5: return vehiculo.getPlaca();
            default: return null;
        }
    }
    
}
