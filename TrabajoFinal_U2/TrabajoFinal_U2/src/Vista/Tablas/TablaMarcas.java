/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vista.Tablas;

import Modelo.Marcas;
import controlador.tda.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaMarcas extends AbstractTableModel{

    private ListaEnlazadaServices<Marcas> lista;

    public ListaEnlazadaServices<Marcas> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Marcas> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Nro";
            case 1: return "MarcaVehiculo";
            case 2: return "Cuidad";
            case 3: return "Numero";
            case 4: return "Registro";
            case 5: return "Patente";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        Marcas marca = lista.obtenerDato(arg0);
        switch(arg1) {
            case 0: return (arg0+1);
            case 1: return marca.getMarca();
            case 2: return marca.getCuidad();
            case 3: return marca.getNumero();
            case 4: return marca.getRegistro();
            case 5: return marca.getPatente();
            default: return null;
        }
    }
    
}
