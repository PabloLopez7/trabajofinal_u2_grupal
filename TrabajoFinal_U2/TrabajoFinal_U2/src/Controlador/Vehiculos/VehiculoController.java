/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Vehiculos;

import Modelo.Vehiculo;
import controlador.tda.lista.ListaEnlazadaServices;

/**
 *
 * @author Usuario
 */
public class VehiculoController {
    private Vehiculo vehiculo;
    private ListaEnlazadaServices<Vehiculo> listaVehiculos;

    public ListaEnlazadaServices<Vehiculo> getListaVehiculos() {
        if(this.listaVehiculos == null)
            this.listaVehiculos = new ListaEnlazadaServices<>();
        return listaVehiculos;
    }

    public void setListaVehiculos(ListaEnlazadaServices<Vehiculo> listaVehiculos) {
        this.listaVehiculos = listaVehiculos;
    }

    public Vehiculo getVehiculo() {
        if(this.vehiculo==null)
            this.vehiculo=new Vehiculo();
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    
}
