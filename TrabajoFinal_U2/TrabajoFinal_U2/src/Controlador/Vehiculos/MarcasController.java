/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Vehiculos;

import Modelo.Marcas;
import controlador.tda.lista.ListaEnlazadaServices;

/**
 *
 * @author Usuario
 */
public class MarcasController {
    private Marcas marca;
    private ListaEnlazadaServices<Marcas> listaMarcas;

    public ListaEnlazadaServices<Marcas> getListaMarcas() {
        if(this.listaMarcas ==null)
            this.listaMarcas = new ListaEnlazadaServices<>();
        return listaMarcas;
    }

    public void setListaMarcas(ListaEnlazadaServices<Marcas> listaMarcas) {
        this.listaMarcas = listaMarcas;
    }

    public Marcas getMarca() {
        if(this.marca==null)
           this.marca = new Marcas();
        return marca;
    }

    public void setMarca(Marcas marca) {
        this.marca = marca;
    }
    
    
}
