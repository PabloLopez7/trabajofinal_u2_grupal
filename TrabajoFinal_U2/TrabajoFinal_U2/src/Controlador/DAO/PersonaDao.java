/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.DAO;

import Modelo.Clientes;

/**
 *
 * @author Usuario
 */
public class PersonaDao extends AdaptadorDao<Clientes>{
    
    private Clientes cliente;
   
    public PersonaDao (){
        super(Clientes.class);
    }

    public Clientes getCliente() {
        if(cliente == null)
            cliente = new Clientes();
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }
    
    
    
}
