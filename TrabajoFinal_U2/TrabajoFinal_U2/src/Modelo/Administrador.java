/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Usuario
 */
public class Administrador {
    
    private String usuario="ADMIN123";
    private String contrasenia="ADMIN123";

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        
        this.usuario = usuario;
    }

    public String getContrasenia() {
        
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        
        this.contrasenia = contrasenia;
    }
}
