/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo.TiendaAutos.enums;

/**
 *
 * @author Usuario
 */
public enum TipoDocumento {
    MARCA("Marca"), VEHICULO("Vehiculo");
    private String tipo;

    private TipoDocumento(String tipo) {
        this.tipo = tipo;
    }
    public String getTipo() {
        return this.tipo;
    }
}
