
package Modelo;

/**
 *
 * @author Usuario
 */
public class Gerente {
    
    private String usuario="USER123";
    private String contrasenia ="CONTRA123";
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        
        this.usuario = usuario;
    }

    public String getContrasenia() {
        
        return contrasenia;
    }

    public void setContraseña(String contraseña) {
        this.contrasenia = contraseña;
    }
    
}
